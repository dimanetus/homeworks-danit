"use strict"

/*
1. Опишіть своїми словами що таке Document Object Model (DOM)
DOM (Document Object Model) - це об'єктна модель документа. Коли браузер зчитує код html сторінки він на її основі створює DOM, щоб за допомогою JavaScript можна було взаємодіяти та маніпулювати ним: шукати елемент, змінювати його, додавати нові елементи і інше. DOM схожий по своїй структурі на дерево, яке складається з вузлів Node, які в свою чергу можуть складатися з вбудованих вузлів, елементів, тексту/коментаря і так безліч разів.

2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
innerText - показує вct текстове наповнення яке лежить між відкриваючими та закриваючими html тегами елемента. Тобто все, що не відноситься до синтаксису html. Якщо всередині є інші html елементи зі своїм наповленням, то innerText проігнорує html і поверне тільки наповлення.
innerHTML - же виведе весь html елемент ціляком - сам текст та розмітку html.

3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
5 найпоширеніших способів звернутися до html елемента за допомогою JS - querySelectorAll, querySelector, getElementById, getElementsByTagName, getElementsByClassName. Який з них використовувати залежить від цілей та особистих вподобань розробника. QuerySelectorAll, querySelector вважаються більш сучасними і можуть повністю замінити функціонал інших. 
*/

// Task1:  Два варіанти
let bgcolorForP = [... document.getElementsByTagName("p")];
console.log(bgcolorForP);
bgcolorForP.forEach(el => el.style.backgroundColor = '#ff0000')

// for (const key of bgcolorForP) {
//     console.log(key);
//     key.style.backgroundColor = '#ff0000'
        
//     }

// Task2: 
const IdOptionList = document.getElementById("optionsList");
console.log(IdOptionList);
let parent = IdOptionList.parentNode;
console.log(parent);

if(IdOptionList.hasChildNodes()) {
    IdOptionList.childNodes.forEach(el => console.log(`Node Name - ${el.nodeName} and Node Type is ${el.nodeType}`))
}

// Task3:
const IdtestParagraph = document.getElementById("testParagraph");
IdtestParagraph.innerHTML = '<p>This is a paragraph<p/>';

// Task4:
const liInMainHeader = document.querySelectorAll(".main-header li");
console.log(liInMainHeader);
for (const li of liInMainHeader) {
    li.classList.add("nav-item")
    console.log(li);
};

// Task5: Два варіанти
const elClassSectionTitle = [... document.getElementsByClassName("section-title")];
elClassSectionTitle.forEach(el => {
    el.classList.remove("section-title");
    console.log(el);
})

// for (const el of elClassSectionTitle) {
//     el.classList.remove("section-title")
//     console.log(el);
// };
