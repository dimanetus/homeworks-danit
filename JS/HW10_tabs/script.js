"use strict";

// 1 варіант
const tabs = document.querySelectorAll(".tabs-title");
const tabText = document.querySelectorAll(".tabs-content > li");

tabs.forEach((el) => {
  el.addEventListener("click", function (event) {

    console.log(event.target.getAttribute('data-li'));
    tabs.forEach((el) => {
      el.classList.remove("active");
    });
    const liId = event.target.getAttribute("data-li");
    let activeLi = document.querySelector(liId);
    tabText.forEach((el) => {
      el.classList.remove("active");
    });
    activeLi.classList.add("active");
    el.classList.add("active");
  });
});



// 2 варіант через делегування
/*
const tabsUl = document.querySelector(".tabs");
const tabText = document.querySelector(".tabs-content");

function onTabClick(event) {
  let target = event.target;

  if (target.tagName != "LI") return;

  const liId = target.getAttribute("data-li");
  let activeLi = document.querySelector(liId);

  let childs = [...tabsUl.children];
  childs.forEach(function (elem) {

    elem.classList.remove("active");
    target.classList.add("active");
  });

  let textChilds = [...tabText.children];

  textChilds.forEach(function (elem) {

    elem.classList.remove("active");
    activeLi.classList.add("active");
  })

}

tabsUl.addEventListener("click", onTabClick);

*/
