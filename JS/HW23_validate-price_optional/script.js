"use strict";

/*

1. Опишіть своїми словами, що таке обробник подій.
Обробка події проводиться за допомогою спеціально призначеної для цього функції.
За його допомогою ми можемо при настанні любої події від користувача (будь-який рух мишою, клік, та натискання клавіатури, фокус і т.д.) виконати корисний для нас код. Ми можемо перезаписувати стандартну поведінку браузерів на ці події і видозмінювати їх на власний розсуд. 

2. Опишіть, як додати обробник подій до елемента. Який спосіб найкращий і чому?
Існує 3 способи додати обробник подій: 
- використання атрибута HTML (<input value="Натисни мене" onclick="alert('Клік!')>");
- Використання властивостей DOM-об’єкта (elem.onclick = function() {})
- метод addEventListener (element.addEventListener(event, handler, [options]);)

Найкращим вважається останній, оскільки на перші 2 способи не дають повісити кілька обробників для однієї події. addEventListener же ми можемо задавати скільки завгодно разів + в нього є багато зручних та корисних внутрішніх методів.
*/

const form = document.createElement("form");
form.style.cssText = `
  text-align: center; 
  padding-top: 35px;
  padding-bottom: 35px;
  background-color: palegoldenrod;`;
document.body.prepend(form);

const input = document.createElement("input");
input.type = "text";
input.id = "price";
input.style.cssText = `
margin-left: 15px;
border: 3px solid grey;
border-radius: 5px;
position: relative;
`;

const label = document.createElement("label");
label.setAttribute("for", "price");
label.textContent = "Price, UAH";
form.append(label, input);

// вводити в інпут можливо тільки цифри
input.addEventListener("keypress", (e) => {
  let regex = /\D/;
  if (regex.test(e.key)) {
    e.preventDefault();
  }
});
input.addEventListener("focus", () => input.classList.add("focus"));
input.addEventListener("blur", onBlur);
input.addEventListener("submit", onBlur);
document.addEventListener("click", (e) => {
  if (e.target.classList.contains("deletePrice")) {
    e.target.closest("span").remove();
    input.value = "";
  }
});

function onBlur() {
  input.classList.remove("focus");
  let currentPrice = document.createElement("span");
  if (input.value.length < 1) {
    return;
  }

  if (input.value < 0) {
    currentPrice.classList.add("enterCorrectPrice");
    currentPrice.innerHTML = `Please enter correct price  <button class="deletePrice">X</button>`;
    input.after(currentPrice);
    input.style.border = `3px solid red`;
  } else {
    currentPrice.classList.add("currentPrice");
    currentPrice.innerHTML = `Поточна ціна: ${input.value} UAH
    <button class="deletePrice">X</button>`;

    input.before(currentPrice);
    input.style.border = `3px solid green`;
    input.style.color = "green";
  }
}

