"use strict";

/*
1. Опишіть своїми словами різницю між функціями `setTimeout()` і `setInterval(`)`.
setTimeout дозволяє викликати функцію чи код тільки ОДИН раз через установлений проміжок часу.
А setInterval дозволяє викликати функцію чи код знову і знову через встановлений інтервал часу. Синтаксис в них однаковий

2. Що станеться, якщо в функцію `setTimeout()` передати нульову затримку? Чи спрацює вона миттєво і чому?
  Функція буде виконуватись відразу після виконання поточного коду.

3. Чому важливо не забувати викликати функцію `clearInterval()`, коли раніше створений цикл запуску вам вже не потрібен?
setInterval буде виконуватися безкінечно якщо його не перервати за допомогою clearInterval()
*/

// const startBtn = document.createElement("button");
// startBtn.classList.add("btn");
// startBtn.textContent = "Відновити показ";
// const stopBtn = document.createElement("button");
// stopBtn.classList.add("btn");
// stopBtn.textContent = "Припинити";

// document.querySelector(".buttons").append(stopBtn, startBtn);

// let timer = setInterval(slider, 2500);
// let counter = 1;
// function slider() {
//   let currentImg = document.querySelector(".showed");

//   currentImg.classList.add('fade-out');
//   setTimeout(() => {
//     currentImg.classList.remove("showed");
//     currentImg.classList.remove("fade-out");
//     counter++;
//     if (counter > 4) {
//       counter = 1;
//     }
//     const img = document.querySelector(`.image-${counter}`);
//     img.classList.add("showed");
//  }, 500);
// }

// stopBtn.addEventListener("click", () => clearInterval(timer));
// startBtn.addEventListener("click", () => {
//   timer = setInterval(slider, 2500);
// });


const btn = document.querySelector('.btn');
btn.addEventListener('click', function (ev) {
  let diam = +prompt('Введіть діаметр(px)');
  while (isNaN(diam) || diam === 0) {
    diam = +prompt('Введіть діаметр(px)')
  }
  const create = document.createElement('button');
  create.textContent = 'Намалювати';
  create.classList.add('btn');
  document.body.append(create);
  create.addEventListener('click', function (ev) {
    for (let i = 0; i < 100; i++) {
      const circle = document.createElement('div');
      circle.style.cssText = `
   width: ${diam}px;
   height: ${diam}px;
   border-radius: 50%;
   margin: 5px;
   display: inline-block;
   `
      circle.style.backgroundColor = getRandomColor();
      create.after(circle);
      circle.classList.add('circle');
    }
  })
  document.body.addEventListener('click', function (e) {
    if (e.target.classList.contains('circle')) {
      e.target.remove();
    }
  })
})

function getRandomColor() {
  const r = Math.floor(Math.random() * 255);
  const g = Math.floor(Math.random() * 255);
  const b = Math.floor(Math.random() * 255);

  return `rgb(${r}, ${g}, ${b})`;
}