"use strict";
 
/*
1. Напишіть, як ви розумієте рекурсію. Навіщо вона використовується на практиці?

Рекурсія - це визов функції самої себе. Такий прийом може бути дуже зручним і ефективним для вирішення деяких завдань коли є можливість розбити операцію на кілька менших однотипних. На практиці рекурсію можна використовувати для багатьох завдань замість циклів де це доцільно.
*/


let num = prompt('Enter your number')

function factorial(x) {
    while( x === null || x === "" || !parseInt(x)) {
        x = prompt('Enter your number', num)
    }
    return (x==1) ? 1 : x * factorial(x - 1);
  }

 /* також у вигляді циклу 

  function factorial(x) {
    let result = x;
    for( let i = x; i > 1; i--) {
        result *= (i - 1);
    } 
    return result;
}
*/

let showResult = (x) => alert(factorial(x));
  
showResult(num);