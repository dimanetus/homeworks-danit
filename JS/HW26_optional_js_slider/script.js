"use strict";

const prevBtn = document.querySelector(".prev");
const nextBtn = document.querySelector(".next");

let counter = 1;
let id;
prevBtn.addEventListener("click", function (e) {
  if (counter === 1) {
    counter = 4;
  } else {
    counter--;
  }
  document.querySelector(".active").classList.remove("active");

  let image = document.querySelector(`#img${counter}`);
  image.classList.add("active");
});

nextBtn.addEventListener("click", function (e) {
  if (counter === 4) {
    counter = 1;
  } else {
    counter++;
  }
  document.querySelector(".active").classList.remove("active");
  let image = document.querySelector(`#img${counter}`);
  image.classList.add("active");
});
