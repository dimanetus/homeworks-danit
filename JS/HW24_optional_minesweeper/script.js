"use strict";
startGame();

function startGame() {
  const minesField = document.querySelector(".container");
  let minesOnField = 10;
  let flagsOnField = 10;

  for (let i = 0; i < 64; i++) {
    let grid = document.createElement("div");
    grid.classList.add("grid-item");
    grid.setAttribute("data-index", `${i}`);
    grid.setAttribute("data-mine", "0");
    minesField.append(grid);
  }

  const result = document.createElement("p");
  result.classList.add("res");
  result.innerHTML = `Flags:   <span>${flagsOnField}</span>, Mines: <span>${minesOnField}</span>`;
  minesField.prepend(result);

  const startFromBegin = document.createElement("button");
  startFromBegin.classList.add("restart");
  startFromBegin.textContent = "Почати гру заново";

  const gameOver = document.createElement("p");
  gameOver.textContent = "Game Over";
  gameOver.classList.add("game-over");

  function createMine() {
    let arrOfMines = [];
    for (let i = 1; i <= 10; i++) {
      let randomNum = Math.floor(Math.random() * 64);
      if (!arrOfMines.includes(randomNum)) {
        arrOfMines.push(randomNum);
      } else {
        i--;
      }
    }
    return arrOfMines;
  }

  let mines = createMine();
  console.log(mines);
  const minesCells = document.querySelectorAll(".grid-item");
  mines.forEach((el) => {
    minesCells[el].setAttribute("data-mine", "1");
  });

  let firstClick = 0;

  // логіка відкривання колонок
  function openCell(e) {
    if (e.target.classList.contains("grid-item")) {
      firstClick++;
      if (firstClick === 1) {
        result.after(startFromBegin);
      }

      if (e.target.dataset.mine === "1") {
        mines.forEach((el) => {
          minesCells[
            el
          ].innerHTML = `<i class="fa fa-bomb" aria-hidden="true"></i>`;
        });
        // завершити гру
        minesField.append(gameOver);
        minesField.removeEventListener("click", openCell);
        minesField.removeEventListener("contextmenu", setFlag);
      } else {
        let row = Math.floor(e.target.getAttribute("data-index") / 8);
        let col = Math.floor(e.target.getAttribute("data-index") % 8);
        // console.log(`row = ${row}, col = ${col}`);
        e.target.textContent = countNearMines(row, col);
        if (countNearMines(row, col) === 0) {
          showEmptyCells(row, col);
        }
      }
    }
  }

  function getCoordinatY(elem) {
    let row = Math.floor(elem.getAttribute("data-index") / 8);
    return row;
  }

  function getCoordinatX(elem) {
    let col = Math.floor(elem.getAttribute("data-index") % 8);
    return col;
  }

  // підрахунок мін по сусідніх колонках
  function countNearMines(row, col) {
    let count = 0;
    let arrNoBombs = [];
    for (let x = -1; x <= 1; x++) {
      for (let y = -1; y <= 1; y++) {
        let index = (row + y) * 8 + (col + x);
        if (isBomb(row + y, col + x)) {
          count++;
        }
      }
    }
    return count;
  }

  function showEmptyCells(row, col) {
    let arrNoBombs = [];
    for (let x = -1; x <= 1; x++) {
      for (let y = -1; y <= 1; y++) {
        let index = (row + y) * 8 + (col + x);
        arrNoBombs.push(minesCells[index]);
      }
    }
    const set = new Set(arrNoBombs);
    Array.from(set).forEach((el) => {
      let row = getCoordinatY(el);
      let col = getCoordinatX(el);
      el.textContent = countNearMines(row, col);
    });
  }

  function isBomb(row, col) {
    let index = row * 8 + col;
    return mines.includes(index);
  }

  // логіка втановлення/прибирання флага
  function setFlag(e) {
    e.preventDefault();
    if (e.target.closest(".grid-item")) {
      let gridCell = minesCells[e.target.closest(".grid-item").dataset.index];
      let i = gridCell.children[0];
      if (e.target.textContent.length) {
        return false;
      }
      if (gridCell.children.length === 0) {
        if (flagsOnField === 0) {
          return false;
        }
        flagsOnField--;
        gridCell.innerHTML = `<i class="fa fa-flag" aria-hidden="true"></i>`;
        result.innerHTML = `Flags:   <span>${flagsOnField}</span>, Mines: <span>${minesOnField}</span>`;
      } else {
        flagsOnField++;
        gridCell.removeChild(i);
        result.innerHTML = `Flags:   <span>${flagsOnField}</span>, Mines: <span>${minesOnField}</span>`;
      }
    }
  }

  // обробники подій
  minesField.addEventListener("click", openCell);
  minesField.addEventListener("contextmenu", setFlag);
  startFromBegin.addEventListener("click", function (e) {
    minesField.innerHTML = "";
    startGame();
  });
}