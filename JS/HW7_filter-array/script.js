"use strict"

/*
1. Опишіть своїми словами як працює метод forEach.
    Метод forEach дозволє легко і зручно робити перебір массивів. Синтаксис «arr.forEach(callback[, thisArg]). В своїй роботі він для кожного елемента масива визиває колбек-функцію один раз, яка приймає три параметри (item, i, arr). По суті метод forEach нічого не повертає, він просто використовується для перебора по аналогії зі звичайними циклами. 

2. Як очистити масив? 
Найкращий спосіб очистити массив - arr.length=0 та arr.splice(0, arr.length). Таким чином ми повністю і безвозвратно стираємо всі значення.

3. Як можна перевірити, що та чи інша змінна є масивом?
З масивами не працює правильно властивість typeof, тому що typeof масива буде object. Тому існує інший метод перевірки на масив - Array.isArray(). Він перевіряє чи є переданий параметром об'єкт масивом чи ні. Якщо масив то повертає true, інакше false.
*/



function filterBy(arr, type) {
    return arr.filter((item) => typeof item != type);
}

// також рішення за допомогою цикла

// function filterBy1(arr, type) {
//     let newArr = [];
//     for (const elem of arr) {
//         if(typeof elem != type) {
//             newArr.push(elem)
//         }
//     }
//     return newArr;
// }

console.log(filterBy(['hello', 'world', 23, '23', {}, null], 'string'));
console.log(filterBy(['hello', 'world', 23, '23', null, 233, false, 45, "3546"], 'number'));

// console.log(filterBy1(['hello', 'world', 23, '23', {}, null], 'string'));
// console.log(filterBy1(['hello', 'world', 23, '23', null, 233, false, 45, "3546"], 'number'));

