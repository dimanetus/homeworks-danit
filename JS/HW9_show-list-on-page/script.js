"use strict";

/*
1. Опишіть, як можна створити новий HTML тег на сторінці.
В JS створити новий HTML тег можна за допомогою  методу  document.createElement(tag). 

2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
Перший параметр методу insertAdjacentHTML дозволяє нам вказати куди саме буде додано новий html-текст. Існує 4 варіанти:
"beforeBegin" - вставка перед елементом.
"afterBegin" - в середину вибраного елемента в самий початок(одразу після відкриваючого тегу) 
"beforeEnd" - в середину вибраного елемента -  в кінець (перед закриваючим тегу)  
"afterEnd" - одразу після елеемнта.

3. Як можна видалити елемент зі сторінки?
Для видалення елементу є метод node.remove().
Також можна використати синтаксис node.innerHTML = ""
*/



function showList(arr, parent) {
  const list = document.createElement("ul");
  arr.map((el) => {
    let elem = "";
    if (!Array.isArray(el)) {
      elem = document.createElement("li");
      elem.textContent = el;
    } else {
      elem = document.createElement("ul");
      showList(el, elem);
    }
    list.append(elem);
  });

  if (arguments.length < 2) {
    document.body.prepend(list);
  } else {
    parent.append(list);
  }
}

let arr1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
let arr2 = ["1", "2", "3", "sea", "user", 23];
let weekDays = [
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
  "Sunday",
];
let inArr = [
  "Kharkiv",
  "Kiev",
  ["Borispol", "Irpin"],
  "Odessa",
  "Lviv",
  "Dnieper",
];

const section = document.querySelector("#section");
const div = document.querySelector(".div");
const heading = document.querySelector("h1");
const p = document.querySelector("p");
const ul_list = document.querySelector(".ul-list");

showList(arr1, section);
showList(arr2);
showList(weekDays, div);
showList(inArr, div);

// для наглядності таймер виділяємо на сторінці
const counter = document.createElement("div");
document.body.prepend(counter);
counter.style.cssText =
"font-size: 100px ; background-color: red; text-align: center";

// зворотній відлік
const deadline = new Date().setSeconds(new Date().getSeconds() + 3);
const countdown = setInterval(function () {
  let timeRemain = Math.ceil((deadline - new Date().getTime()) / 1000) ;
  counter.textContent = timeRemain;
}, 1000);


setTimeout(() => document.body.innerHTML = "", 3000);
countdown;
