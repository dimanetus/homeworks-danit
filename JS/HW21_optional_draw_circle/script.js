"use strict";

// трохи добавив стилів для краси

const btnDraw = document.querySelector(".btn_create");
btnDraw.style.cssText = `font-size: 35px; margin-left: auto; margin-right: auto; margin-bottom: 30px; display: block`;
let circleContainer;

btnDraw.addEventListener("click", drawCircle, { once: true });
function drawCircle() {
  const form = document.createElement("form");
  form.style.cssText = `text-align: center;`;
  btnDraw.after(form);

  const diametr = document.createElement("input");
  diametr.type = "text";
  diametr.id = "diametr";

  const diaLabel = document.createElement("label");
  diaLabel.setAttribute("for", "diametr");
  diaLabel.textContent = "Діаметр кола в пікселях";
  diaLabel.style.cssText = `margin-right: 20px;`;

  const submitBtn = document.createElement("button");
  submitBtn.type = "submit";
  submitBtn.innerText = "Намалювати";
  submitBtn.style.cssText = `display: block; width: 200px; margin: 10px auto 20px;`;
  form.append(diaLabel);
  form.append(diametr);
  form.append(submitBtn);

  // створюються 100 кіл не тільки по кнопці "Намалювати" а і enter
  document.addEventListener("submit", createOneHundredCircle);

  circleContainer = document.createElement("div");
  document.body.append(circleContainer);
}

function getRandomColor() {
  const r = Math.floor(Math.random() * 255);
  const g = Math.floor(Math.random() * 255);
  const b = Math.floor(Math.random() * 255);
  return `rgb(${r}, ${g}, ${b})`;
}

function createOneHundredCircle(e) {
  e.preventDefault();
  // коли на екрані вже є кола, і змінити в полі вводу число і натиснути submit то розмір кіл зміниться 
  circleContainer.innerHTML = "";
  circleContainer.style.cssText = `display: grid;  grid-template-columns: repeat(10, 1fr)`;
  for (let i = 0; i < 100; i++) {
    function createCircle(a) {
      const div = document.createElement("div");
      div.classList.add("circle");
      circleContainer.append(div);
      div.style.cssText = `height: ${a}px; width: ${a}px; border-radius: 50%`;
      div.style.backgroundColor = getRandomColor();
    }
    createCircle(diametr.value);
  }
  document.addEventListener("click", function (e) {
    if (!e.target.classList.contains("circle")) {
      return;
    } else e.target.remove();
  });
}
