"use strict";

const eyeIcon1 = document.querySelector("#eye1");
const eyeIconSlash1 = document.querySelector("#eye_slash1");
const eyeIcon2 = document.querySelector("#eye2");
const eyeIconSlash2 = document.querySelector("#eye_slash2");

eyeIconSlash1.style.display = "none";
eyeIconSlash2.style.display = "none";

eyeIcon1.addEventListener("click", showPass);
eyeIconSlash1.addEventListener("click", hidePass);
eyeIcon2.addEventListener("click", showPass);
eyeIconSlash2.addEventListener("click", hidePass);

// замість e.target можна використати this. Так працюватиме також
function hidePass(e) {
  e.target.closest(".input-wrapper").children[0].type = "password";
  e.target.style.display = "none";
}

function showPass(e) {
  e.target.previousElementSibling.type = "text";
  e.target.nextElementSibling.style.display = "block";
}

const input1 = document.querySelector("input");
const input2 = document.querySelector("label:last-of-type > input");
const btnSubmit = document.querySelector("button");

btnSubmit.addEventListener("click", (e) => {
  e.preventDefault();
  if (input1.value === input2.value) {
    alert(`You are welcome`);
  } else {
    const p = document.createElement('p');
    p.textContent = `Потрібно ввести однакові значення`;
    p.style.color = 'red';
    btnSubmit.insertAdjacentElement("beforebegin", p);
  }
});
