"use strict";

function filterCollection(arr, string, isStrict, ...keys) {
  const result = [];
  for (const obj of arr) {
    let data;
    for (const key of keys) {
      const value = getValueByKey(obj, key);
      if (
        isStrict &&
        string
          .split(" ")
          .every((el) => value.toLowerCase().includes(el.toLowerCase()))
      ) {
        data = obj;
        break;
      }
      if (
        !isStrict &&
        string
          .split(" ")
          .some((el) => value.toLowerCase().includes(el.toLowerCase()))
      ) {
        data = obj;
        break;
      }
    }

    if (data) {
      result.push(obj);
    }
  }
  return result;
}

function getValueByKey(obj, key) {
  console.log("obj", obj);
  const keyLevels = key.split(".");
  let res = obj;

  keyLevels.forEach((key) => {
    if (Array.isArray(res)) {
      res = res.map((el) => el[key]).join(" ");
    } else {
      res = res?.[key] || "";
    }
  });

  return res;
}

const vehicles = [
  {
    name: "Toyota Bob",
  },
  {
    description: "bob 12345",
  },
  {
    contentType: {
      name: "Tony",
    },
  },
  {
    locales: [
      {
        name: "bob",
        data: "12345",
      },
      {
        name: "John",
      },
    ],
  },
];

const dataA = filterCollection(
  vehicles,
  "Bob Toyota",
  true,
  "name",
  "description",
  "contentType.name",
  "locales.name",
  "locales.data",
  "locales.description"
);

console.log("dataA", dataA);

const dataB = filterCollection(
  vehicles,
  "Bob Toyota",
  false,
  "name",
  "description",
  "contentType.name",
  "locales.name",
  "locales.description"
);

console.log("dataB", dataB);