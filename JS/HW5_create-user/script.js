"use strict"

/*
1. Опишіть своїми словами, що таке метод об'єкту
Метод об'єкту - це функція внутрі об'єкта, яка дозволяє виконувати маніпуляції зі значенням свойств об'єкта. Вони працюють виключно всередині цього об'єкта. 
Існує безліч встроєних об'єктів, які доступні кожному об'єкту від батьківського конструктора Object. Ну і ми можемо створювати власні методи.

2. Який тип даних може мати значення властивості об'єкта?
Значення властивості може бути будь-якого типу - примітивні типи данних, об'єкти, функції та змінною, в якій зберігають дані.

3. Об'єкт це посилальний тип даних. Що означає це поняття?
Це означає, що коли об'єкт присвоюється змінній, то в цій змінній не зберігається цей об'єкт а лише посилання на об'єкт. І при зміні будь-яких властивостей об'єкта вони автоматично змінюються скрізь куди веде це посилання. Це кардинальна відмінність посилальних типів данних від примітивних. 

*/

function createNewUser() {

let name = prompt('What is your name?');
let surname =  prompt('What is your lastname?');
const newUser = {
    _firstName: name,
    _lastName: surname,
    getLogin: function() {
        return (this._firstName[0] + this._lastName).toLowerCase()
    },

    set setFirstName(value) {
        this._firstName = value;
    },
    set setLastName(value) {
    this._lastName = value;
    },
    };
    Object.defineProperty( newUser, 'firstName' , {
        writable : false
        });
    Object.defineProperty( newUser, 'lastName' , {
            writable : false
            });
        return newUser;
}


const veryFirstUser = createNewUser();
console.log(veryFirstUser);
console.log(veryFirstUser.getLogin());












