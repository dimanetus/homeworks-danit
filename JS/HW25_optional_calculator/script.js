"use strict";

const buttons = document.querySelector(".keys");
const display = document.querySelector("input[type=text]");
const inMemory = document.createElement("p");
inMemory.classList.add("memory");
inMemory.textContent = "m";
document.querySelector(".display").append(inMemory);
buttons.addEventListener("click", clickOnMouse);

let expresion = "";
let firstDig = "";
let secondDig = "";
let oper = "";
let res;
let memoryNum;

function clickNumber(e) {
  if (!expresion || expresion === "0") {
    display.value = e.target.value;
    firstDig = e.target.value;
    expresion = e.target.value;
  } else {
    if (!oper) {
      display.value += e.target.value;
      firstDig += e.target.value;
      expresion += e.target.value;
    } else {
      display.value += e.target.value;
      secondDig += e.target.value;
    }
  }
}

function clickOperator(e) {
  if (!expresion) {
    return;
  }
  oper = e.target.value;
}

function calculate() {
  console.log(firstDig);
  console.log(secondDig);
  console.log(oper);
  if (firstDig && secondDig) {
    switch (oper) {
      case "+":
        res = +firstDig + +secondDig;
        break;
      case "-":
        res = +firstDig - +secondDig;
        break;
      case "*":
        res = +firstDig * +secondDig;
        break;
      case "/":
        res = +firstDig / +secondDig;
        break;
    }
    display.value = res;
    firstDig = res;
    secondDig = "";
  }
};


function memory() {
  memoryNum = display.value;
  inMemory.classList.add("active");
  console.log(memoryNum);
}

function clickOnMouse(e) {
  if (e.target.tagName === "INPUT") {
    if (e.target.value.match(/[0-9.]/)) {
      // можна зробити обмеження, щоб число поміщалося в дисплей. При роботі  великими числами виникають проблеми
      // if(display.value.length >= 18) {
      //   return
      // }
      clickNumber(e);
    }
    if (e.target.value.match(/[*/+-]/)) {
      if (firstDig && secondDig) {
        calculate();
      }
      clickOperator(e);
    }
    if (e.target.value === "C") {
      display.value = "";
      firstDig = "";
      secondDig = "";
      oper = "";
    }
    if (e.target.value === "=") {
      calculate();
    }
    if (e.target.value === "m-" || e.target.value === "m+") {
      memory();
    }
    if (e.target.value === "mrc") {
      if (memoryNum) {
        secondDig = memoryNum;
        display.value += memoryNum;
        memoryNum = "";
      } else {
        inMemory.classList.remove("active");
      }
    }
  }
}

document.addEventListener("keypress", clickOnKey);

function clickOnKey(e) {
  let regexDig = /[0-9]/;
  let regex = /[+-/*]/;
    if (regexDig.test(e.key)) {
      clickNumberKey(e);
    };
    if (regex.test(e.key)) {
      clickOperKey(e);
    };
    if (e.key === "Enter" || e.key === '=') {
      e.preventDefault();
      calculate();
    }
  };

function clickOperKey(e) {
  if (!expresion) {
    return;
  }
  oper = e.key;
}

function clickNumberKey(e) {
  if (!expresion || expresion === "0") {
    display.value = e.key;
    firstDig = e.key;
    expresion = e.key;
  } else {
    if (!oper) {
      display.value += e.key;
      firstDig += e.key;
      expresion += e.key;
    } else {
      display.value += e.key;
      secondDig += e.key;
    }
  }
}
