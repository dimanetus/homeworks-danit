"use strict";

const table = document.createElement("table");
table.classList.add("table");
document.body.append(table);

for (let i = 0; i < 10; i++) {
  let tr = document.createElement("tr");
  for (let j = 0; j < 10; j++) {
    let td = document.createElement("td");
    td.classList.add("cell");
    tr.append(td);
  }
  table.append(tr);
}

table.addEventListener("click", function (e) {
  if (e.target.classList.contains("cell")) {
    e.target.classList.toggle("bgc");
  }
});

document.body.addEventListener("click", function (e) {
  if (!e.target.closest("table")) {
    table.classList.toggle("bgc");
// тут без цикла я ніяк не зміг змінювати вибрані елементи на протилежний колір так щоб вони працювали n-ну кількість разів. 
    document.querySelectorAll("tr .bgc").forEach(function (el) {
      el.classList.toggle("bgc1");
    });
   
  }
});
