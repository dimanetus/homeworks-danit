"use strict";

/*Чому для роботи з input не рекомендується використовувати клавіатуру?

Тому, що події клавіатури не повністю покривають всі дії які можна зробити з input. Якщо для заповлення input використати copy/cut/paste то обробники події клавітури на них не спрацюють. Тому краще використовувати спеціальні події для input : change, input, cut, copy, paste

*/

const btns = document.querySelectorAll('button');
document.addEventListener('keypress', showLetter);

function showLetter(e) {
  if(e.key === "s" || e.key === "e"|| e.key === "o" || e.key === "n" || e.key === "l" || e.key === "z" || e.key === "Enter") {
  } 
  btns.forEach(el => {
    el.style.backgroundColor = "#000000"
  })
  document.getElementById(e.key).style.backgroundColor = "blue";
  }

// 2 варіант за допомогою регулярних виразів
//   function showLetter(e) {
// let regex = /[seonlz]/;
// if(regex.test(e.key) || e.key === "Enter") {
//   btns.forEach(el => {
//       el.style.backgroundColor = "#000000"
//     })
//     document.getElementById(e.key).style.backgroundColor = "blue";}
//   }