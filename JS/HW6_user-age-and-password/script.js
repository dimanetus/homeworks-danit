"use strict"

/*
1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
Екранування - це спеціальний символ \ (зворотня коса риска) в програмуванні який використовується в строках та регулярних виразах. Цей символ якби каже інтерпритатору js, що зараз піде символ який наче вирваний із нормального потоку символів і має своє особливе значення. 
Цей символ ми можемо використати коли нам потрубно поставити апостроф чи лапки в строці, бо без \ JS інтерпретерував це як закінчення поточної строки. І також \ використовуться коли потрібно використати регулярні вирази.

2. Які засоби оголошення функцій ви знаєте?
Мені знайомі 3 способи оголошення функцій - Function Declaration, Function Expression и Named Function Expression. Також сюди можна добавити стрілочну функцію, але не певен, що це йде як окремий спосіб оголошення функції(її можна внести в function expression). Також зустрічай синтаксис newFunction, але поки не знаю як він працює

3. Що таке hoisting, як він працює для змінних та функцій?
Hoisting(підняття) - це спеціальний механізм в JS в якому змінні та функції піднімаються в саму верхню частину своєї області видимості. По факту піднімається тільки обьявлення функції та змінної. Саме тому цей механізм не працює з усіма функціями та змінними. Тільки Function Declaration має механізм підняття і саме тому ми можемо визивати її ще до її обявлення. Якщо говорити про змінні то тут лише var має властивість підніматися вгору області видимості, але лише сама змінна не її значення. Тобто якщо визвати змінну обьявлену з var до її оголошення та ініцалізації то це не викличе помилку а дасть результат undefined. Що стосується let і const то в цьому випадку видасть саме помилку. 

*/

function createNewUser() {

let name = prompt('What is your name?');
let surname =  prompt('What is your lastname?');
let birthday = prompt("Enter your date of birth - dd.mm.yyyy")
const newUser = {
    _firstName: name,
    _lastName: surname,
    birthday: birthday,
    getLogin: function() {
        return (this._firstName[0] + this._lastName).toLowerCase()
    },

    set setFirstName(value) {
        this._firstName = value;
    },
    set setLastName(value) {
    this._lastName = value;
    },
    getAge: function() {
        let today = new Date();
        let birthDate = new Date(this.birthday.split(".").reverse().join("."));
        return today.getFullYear() - birthDate.getFullYear();
    },
    getPassword: function() {
        let yearOfBirth = this.birthday.split(".");
        return this._firstName[0].toUpperCase() + this._lastName + yearOfBirth[2]
    }
    };

    Object.defineProperty( newUser, 'firstName' , {
        writable : false
        });
    Object.defineProperty( newUser, 'lastName' , {
            writable : false
            });

        return newUser;
      
}


const veryFirstUser = createNewUser();
console.log(veryFirstUser);
console.log(veryFirstUser.getAge());
console.log(veryFirstUser.getPassword());







