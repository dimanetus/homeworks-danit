"use strict";

const btn = document.createElement('button');
btn.classList.add('change-theme');
btn.textContent = 'Змінити тему';
document.querySelector('.container').prepend(btn);


let curTheme = document.querySelector('#theme');

btn.addEventListener('click', () => {
let currentLink = curTheme.getAttribute('href');
  const darkTheme = './css/style_dark.css'
  const lightTheme = './css/style_white.css'

  if(currentLink === darkTheme) {
    currentLink = lightTheme;
    } else{
      currentLink = darkTheme
    } 
    curTheme.setAttribute('href', currentLink);
    localStorage.setItem('theme', currentLink);
  } )


// вносимо вибрану тему в localStorage і кожного разу при загрузці робимо назначаємо останню версію

let themeFromStorage = localStorage.getItem('theme');
  curTheme.setAttribute('href', themeFromStorage);

