"use strict";

const root = document.querySelector('#root');
const url = 'https://api.ipify.org/?format=json';
const adressUrl = `http://ip-api.com/json/`;


async function findIP() {
   try {
      const userIp = await fetch(url);
      const { ip } = await userIp.json();
      // console.log(ip)
      const getAddress = await fetch( adressUrl + `${ip}` + `?fields=status,continent,country,regionName,city,district`);
      // const res = await getAddress.json();
      const { status, continent, country, regionName, city, district } = await getAddress.json();
      // console.log(status,continent,  country, regionName, city, district);

      root.insertAdjacentHTML('beforeend', `
      <div class='adress'>
         <h2>Your current location:</h2>
         <p>Continent - ${continent}</p>
         <p>Country - ${country}</p>
         <p>Region - ${regionName}</p>
         <p>District - ${district}</p>
      </div>`)
   } catch (error) {
      console.error(error);
   }
}

const btn = document.querySelector(".btn")
btn.addEventListener('click',findIP)