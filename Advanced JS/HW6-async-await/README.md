## Теоретичне питання
Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript

Асинхронність в JS - це можливість працювати з декількома задачами одночасно (багатопотоковість). 
Треба розрізняти синхронність в програмуванні та асинхронність. У синхронних програмах виконання коду уже в строгій послідовності. Тобто спочатку спрацьовує перший рядок, за ним другий і т.д. При цьому рядок 2 не зможе запуститися доки 1 рядок не завершиться.
В асинхронних програмах, ці самі два рядки(Рядок2 після Рядка1) ожуть виконкуватися окромо. В 1 рядку можуть бути команди, які будуть виконуватися в майбутньому, а 2 рядок працює до завершення команди в Рядок1.

