"use strict";

const URL = "https://ajax.test-danit.com/api/swapi/films";

async function fetchFilms(url) {
  let response = await fetch(url);
  let filmList = await response.json();
  return filmList;
}

function renderList(array) {
  const newLi = document.createElement("ul");
  array.forEach((elem) => {
    newLi.insertAdjacentHTML("beforeend", `<li>${elem}</li>`);
  });
  return newLi;
}

function renderCharacters(arr) {
  arr.forEach(({ characters }, idx) => {
    const charsPromise = characters.map((personage) => {
      return fetchFilms(personage).then(({ name }) => name);
    });

    Promise.allSettled(charsPromise)
      .then((resolve) => {
        return resolve.map(({ value }) => value);
      })
      .then((charArr) => {
        const li = document.querySelector("ul");
        li.children[idx].append(renderList(charArr));
      });
  });
}

fetchFilms(URL)
  .then((filmArr) => {
    document.body.append(
      renderList(
        filmArr
          .map(({ name, episodeId, openingCrawl }) => {
            return `<b>Episode ${episodeId}: ${name}</b>
          <br>${openingCrawl}<br> Characters:`;
          })
          .sort()
      )
    );
    return filmArr;
    // тут маємо виведений в браузері список усіх фільмів на екрані
  })
  // додаємо до кожного списку інформацію про персонажів по мірі загрузки
  .then((filmArr) => {renderCharacters(filmArr)});


