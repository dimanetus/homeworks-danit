"use strict"

// Вважається гарним тоном використовувати конструкцію try...catch коли відбуваються запити на сервер, обробка данних від користувача, завантаження або вікриття файлів.

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

const props = ['author', 'name', 'price'];

function listAdder(arr, props) {
  const ulList = document.createElement('ul');
  arr.forEach((obj, i) => {
     try {
        isPropInObj(obj, props, i);
        let innerLi = createList(arr[i]);
        ulList.insertAdjacentHTML('beforeend', `${innerLi}`);
     } catch (error) {
        console.log(error);
     }
  })
  document.querySelector('#root').append(ulList)
};

function isPropInObj(obj, props, index = 0) {
  props.forEach((prop) => {
      if (!obj.hasOwnProperty(prop)) {
         throw new SyntaxError(`Відсутня властивість ${prop} у об'єкта №${index + 1}`)
      }
   })
};

function createList(obj) {
   let list = '';
   for (const [key, value] of Object.entries(obj)) {
      list += `${key} - ${value}, `;
   }
   return `<li>${list}</li>`;
}

listAdder(books, props);
