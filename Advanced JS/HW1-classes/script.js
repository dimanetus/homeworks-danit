class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }
  set name(value) {
    this._name = value;
  }
  get name() {
    return this._name;
  }
  set age(value) {
    this._age = value;
  }
  get age() {
    return this._age;
  }
  set salary(value) {
    this._salary = value;
  }
  get salary() {
    return this._salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }

  get salary() {
    return this._salary * 3;
  }

  set lang(value) {
    this._lang = value;
  }

  get lang() {
    return this._lang;
  }
}

const programmer1 = new Programmer("Michael", 25, "500", ["JS", "HTML", "CSS"]);
const programmer2 = new Programmer("Dmytro", 30, "1000",  ["JS", "Python", "Java"]);
const programmer3 = new Programmer("Mykola", 22, "1500", ["C#", "Python", "Ruby"]);

console.log(programmer1, programmer1.salary);
 console.log(programmer2);
 console.log(programmer3);
