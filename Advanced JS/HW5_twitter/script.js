"use strict";

class Card {
  constructor(usersUrl, postsUrl) {
    this.usersURL = usersUrl;
    this.postsURL = postsUrl;
  }

  async renderTwit() {
    try {
    const authors = await request("GET", this.usersURL),
          posts = await request("GET", this.postsURL),
          root = document.querySelector("#root");
    
    posts.forEach(({ id, userId, title, body }) => {
      const newPost = document.createElement("div");
      newPost.id = `${id}`;
      newPost.classList.add("twit", `post-${id}`);
      newPost.innerHTML = `
                <div class="twit-wrapper">
                      <h2 class="twit-author">
                        ${authors.find((author) => author.id === userId).name} <span class="twit-email"> ${authors.find((author) => author.id === userId).email} </span>
                      </h2>
                    <div class="post-main">
                        <div class="twit-title">${title}</div>
                        <div class="twit-text">${body}</div>
                    </div>
                </div>
                <button class="twit-btn" data-id=${id}>Delete post</button>`;

                root.append(newPost);
    });
    return document.querySelectorAll(".twit-btn")
    } catch (err) {
      console.error(err)
    }
  }

  deletePosts(elem) {
    elem.forEach((btn) => {
      btn.addEventListener("click", async (event) => {
        let id = btn.getAttribute("data-id");
        let postToDelete = await request("DELETE", this.postsURL + "/" + id);
        if (postToDelete.ok === true) {
          const removePost = document.getElementById(`${id}`);
          removePost.remove();
        }
      });
    });
  }
}

async function request(method, url) {
 try {
  const headers = {
    "Content-Type": "application/json",
  };
  const result = await fetch(url, {method: method, headers: headers});
  // console.log(result)
  if (method === "DELETE") {
    return await result;
  } else {
    return await result.json();
  }
 } catch (err) {
  console.error(err)
}

}


const usersURL = `https://ajax.test-danit.com/api/json/users`,
      postsURL = `https://ajax.test-danit.com/api/json/posts`;

const card = new Card( usersURL, postsURL);

card.renderTwit()
    .then((item) => {
        card.deletePosts(item);
      });
